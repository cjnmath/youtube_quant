#! /usr/bin/python3

import sys
from utiles import change_to_downloaded_status

url = sys.argv[1]
if change_to_downloaded_status(url):
    print("OK, already skipped")
