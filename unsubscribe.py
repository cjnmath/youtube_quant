#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python
import sys
from utiles import unsubscribe_rss
from utiles import get_playlist_rss

url = sys.argv[1]
if url.startswith('https://www.youtube.com/playlist?list='):
    print("Wait!!! This is a playlist url, I need to converted it to rss url...")
    url = get_playlist_rss(url)
if not unsubscribe_rss(url):
    print("Something Wrong!")
else:
    print("Successfully unsubscribe!")
