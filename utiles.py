#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python
import sqlite3
import time
import os
import requests
from lxml import etree
from lxml.etree import fromstring
from lxml import html
import re
import subprocess
import setting
import json

script_dir = os.path.dirname(os.path.relpath(__file__))
db_dir = os.path.join(script_dir, 'data.db')
back_up_dir = os.path.join(script_dir, '.data.backup.db')
proxy_server = setting.PROXY_SERVER

proxies = {
    'http': 'socks5h://{}'.format(proxy_server),
    'https': 'socks5h://{}'.format(proxy_server)
}
headers = {
    'Host': 'www.youtube.com',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0',
    'Accept': 'text/html,application/xhtml+xm plication/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1'
}
namespaces = {
    'main': 'http://www.w3.org/2005/Atom',
    'media': 'http://search.yahoo.com/mrss/',
    'yt': 'http://www.youtube.com/xml/schemas/2015'
}


def clean_special_char(line):
    line = re.sub(r'/', '-', line)
    line = re.sub(r'\|', '-', line)
    return line


def back_up_db():
    command_a = ['touch', back_up_dir]
    command_b = ['cp', db_dir, back_up_dir]
    subprocess.call(command_a)
    subprocess.call(command_b)
    print("[---] Sucessfully backup database!")


def check_make_db():
    if not os.path.exists(db_dir):
        open(db_dir, 'w').close()
        if os.path.exists(back_up_dir):
            commad = ['cp', back_up_dir, db_dir]
            subprocess.call(commad)
        else:
            query_1 = """
                CREATE TABLE download_log (url TEST, video_name TEST, download_status BOOLEAN, from_channel TEST, playlist_rss TEST, modified_date DATETIME);
            """
            query_2 = """
                CREATE TABLE subscribe_log (url TEST, channel_name TEST, playlist_name TEST, modified_date DATETIME);
            """
            conn = sqlite3.connect(db_dir)
            cursor = conn.cursor()
            cursor.execute(query_1)
            cursor.execute(query_2)
            conn.commit()
            conn.close()


def load_download_log_urls(download_status=None):
    """
        download_status:
                        1, downloaded
                        0, undownloaded
                        None, all
    """
    check_make_db()
    result = []
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    if download_status is not None:
        query = '''
        SELECT url FROM download_log WHERE download_status=?
        '''
        rows = cursor.execute(query, (download_status,))
        conn.commit()
    else:
        query = '''
        SELECT url FROM download_log
        '''
        rows = cursor.execute(query)
        conn.commit()

    for row in rows:
        result.append(row[0])

    conn.close()
    return result


def write_in_download_log(url, video_name=None, channel_rss=None, playlist_rss=None, download_status=0):
    if not url.startswith('https://www.youtube.com/watch?'):
        print("This is not a youtube video url, please check.")
        return False
    old_urls = load_download_log_urls()
    if url not in old_urls:
        check_make_db()
        conn = sqlite3.connect(db_dir)
        cursor = conn.cursor()
        query = """
            INSERT INTO download_log VALUES (?,?,?,?,?,?);
        """
        t = (url, video_name, download_status, channel_rss, playlist_rss, time.time())
        cursor.execute(query, t)
        conn.commit()
        conn.close()
        # print("")
        return True
    else:
        print("This video url was added before!")
        return False


def delete_from_download_log(url):
    old_urls = load_download_log_urls()
    if url not in old_urls:
        return False
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    query = """
        DELETE FROM download_log WHERE url=?
    """
    cursor.execute(query, (url,))
    conn.commit()
    conn.close()
    return True


def change_to_downloaded_status(url):
    old_urls = load_download_log_urls()
    if url not in old_urls:
        return False
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    query = """
        UPDATE download_log SET download_status=1 WHERE url=?;
    """
    cursor.execute(query, (url,))
    conn.commit()
    conn.close()
    return True


def load_sub_urls(load_channel=True):
    """
        if load_channel=False, load play_list url
    """
    check_make_db()
    result = []
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    query = """
        SELECT url, channel_name, playlist_name FROM subscribe_log;
    """
    rows = cursor.execute(query)
    for row in rows:
        url = row[0]
        channel_name = row[1]
        playlist_name = row[2]
        if load_channel:
            if channel_name:
                result.append(url)
        else:
            if playlist_name:
                result.append(url)
    conn.close()
    return result


def get_playlist_rss(playlist_url):
    playlist_id = re.search('playlist\?list=(PL.*)', playlist_url).group(1)
    return 'https://www.youtube.com/feeds/videos.xml?playlist_id={}'.format(playlist_id)


def subscribe_rss(rss_url):
    if not rss_url.startswith('https://www.youtube.com/feeds/videos.xml?'):
        print("This is not a Youtube RSS URL, please check!")
        return False
    new_playlist_url = None
    new_channel_url = None
    if "playlist_id" in rss_url:
        new_playlist_url = rss_url
        old_sub_urls = load_sub_urls(load_channel=False)
    elif "channel_id" in rss_url:
        new_channel_url = rss_url
        old_sub_urls = load_sub_urls(load_channel=True)

    if rss_url not in old_sub_urls:
        conn = sqlite3.connect(db_dir)
        cursor = conn.cursor()
        query = """
            INSERT INTO subscribe_log VALUES (?,?,?,?);
        """
        r = parse_rss(rss_url)
        t = (rss_url, r[rss_url]['channel_name'], r[rss_url]['playlist_name'], time.time())
        cursor.execute(query, t)
        conn.commit()
        conn.close()

        if new_channel_url:
            download_status = 1
            old_download_url = load_download_log_urls()
            for d in r[rss_url]['datas']:
                if d["video_url"] not in old_download_url:
                    write_in_download_log(
                                url=d["video_url"],
                                video_name=d["video_name"],
                                channel_rss=new_channel_url,
                                playlist_rss=new_playlist_url,
                                download_status=download_status)
        return True
    else:
        print("Already subscribed!!!")
        return False


def unsubscribe_rss(rss_url):
    if not rss_url.startswith('https://www.youtube.com/feeds/videos.xml?'):
        print("This is not a Youtube RSS URL, please check!")
        return False
    new_playlist_url = None
    new_channel_url = None
    if "playlist_id" in rss_url:
        new_playlist_url = rss_url
        old_sub_urls = load_sub_urls(load_channel=False)
    elif "channel_id" in rss_url:
        new_channel_url = rss_url
        old_sub_urls = load_sub_urls(load_channel=True)

    if rss_url not in old_sub_urls:
        print("You haven't subscribed to this channel yet!")
        return False
    else:
        conn = sqlite3.connect(db_dir)
        cursor = conn.cursor()
        query_a = """
            DELETE FROM subscribe_log WHERE url=?;
        """
        t_a = (rss_url,)
        if new_channel_url:
            query_b = """
            DELETE FROM download_log WHERE from_channel=?
            """
        if new_playlist_url:
            query_b = """
            DELETE FROM download_log WHERE playlist_rss=?
            """
        cursor.execute(query_a, t_a)
        cursor.execute(query_b, (rss_url, ))
        conn.commit()
        conn.close()
        return True


def parse_rss(rss_url):
    is_channel = True
    if "?playlist_id=" in rss_url:
        is_channel = False
    result = {}
    result[rss_url] = {}
    session = requests.Session()
    response = session.get(rss_url, headers=headers, proxies=proxies, stream=True)
    parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
    root = fromstring(response.text.encode('utf-8'), parser=parser)
    if is_channel:
        result[rss_url]['channel_name'] = root.xpath(
                                    '//main:title/text()',
                                    namespaces=namespaces
        )[0]
        result[rss_url]['playlist_name'] = None
    else:
        result[rss_url]['channel_name'] = None
        result[rss_url]['playlist_name'] = root.xpath(
                                        '//main:title/text()',
                                        namespaces=namespaces
        )[0]

    result[rss_url]['datas'] = []
    for entry in root.xpath(
                    '//main:entry',
                    namespaces=namespaces):
        d = {}
        name = entry.xpath(
                        './/main:title/text()',
                        namespaces=namespaces)[0]
        name = clean_special_char(name)
        lower_name = name.lower()
        if 'live' in lower_name or 'en direct' in lower_name:
            continue
        d["video_name"] = name
        description = entry.xpath(
                                './/media:description/text()',
                                namespaces=namespaces)
        description = description[0] if description else None
        d["description"] = description
        video_url = entry.xpath(
                    './/main:link/@href',
                    namespaces=namespaces)[0]
        d["video_url"] = video_url
        result[rss_url]['datas'].append(d)

    return result


def get_video_info(url):
    # response = get_response(url, proxies=proxies)
    HEADERS = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0",
        "Referer": url
    }
    response = requests.get(url, headers=HEADERS, proxies=proxies)
    # print(response.text)
    root = html.fromstring(response.text)
    r_scripts = root.xpath('//script/text()')
    for script in r_scripts:
        t = None
        if 'ytplayer.config' in script and 'videoplayback' in script:
            r = re.search('ytplayer\.config \=.+?(\{.+\})\;ytplayer\.load', script)
            if r:
                t = r.group(1)
                d = json.loads(t)
                return d
    return None


def extract_stream_data(video_info):
    r = {}
    if video_info is None:
        return None
    dd = json.loads(video_info['args']['player_response'])
    r['formats'] = dd['streamingData'].get('formats')
    r['adaptiveFormats'] = dd['streamingData'].get('adaptiveFormats')
    return r


def extract_video_detail(video_info):
    return json.loads(video_info['args']['player_response'])['videoDetails']


def extract_480p_itag(streamingData):
    if streamingData is None:
        return None
    for k, v in streamingData.items():
        for i in v:
            qualityLabel = i.get('qualityLabel')
            if not qualityLabel:
                continue
            elif '480p' not in qualityLabel:
                continue
            mimeType = i.get('mimeType')
            if 'mp4' in mimeType:
                itag = i.get('itag')
                return str(itag)
